version 1.0

workflow wf {
  call samtools_view_to_cram
}

task samtools_view_to_cram {

  input {
    File input_file
    File input_file_reference
    Int threads = 2
  }
  
  command {
    set -Eeuxo pipefail
    mkdir -p output/
    
    samtools view \
      -C \
      -@ ${threads} \
      --reference ${input_file_reference} \
      -o output/view.cram \
      ${input_file}
      
  }
  
  runtime {
    cpu: "4"
    memory: "16GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_samtools:1.10"
    preemptible: 5
  }
  
  output {
    File output_file_cram = "output/view.cram"
  }
  
}
