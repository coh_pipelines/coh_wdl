version 1.0

workflow wf {
  call gatk_genotypegvcf
}

task gatk_genotypegvcf {

  input {
    File input_file
    File input_file_index
    File input_file_reference_fasta
    File input_file_reference_fai
    File input_file_reference_dict
    File input_file_gvcf
    File input_file_gvcf_index
    Array[String] intervals
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/
    mkdir -p temp/

    gatk GenotypeGVCFs \
      --tmp-dir "temp" \
      --java-options "-Xmx14G" \
      --reference ${input_file_reference_fasta} \
      --intervals ${sep=" --intervals " intervals} \
      --variant ${input_file_gvcf} \
      --output "output/genotypegvcf.g.vcf.gz"
  }
  
  runtime {
    cpu: "4"
    memory: "16 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_gatk:4.1.5.0"
    preemptible: 5
  }
  
  output {
    File output_file = "output/genotypegvcf.g.vcf.gz"
  }
  
}


  