version 1.0

workflow wf {
  call autodockvina_dock
}

task autodockvina_dock {

  input {
    File input_file_receptor
    Array[File] input_file_ligands
    Float? center_x
    Float? center_y
    Float? center_z
    Float? size_x = 40.0
    Float? size_y = 40.0
    Float? size_z = 40.0
    Int? max_num_modes = 10
    Int? seed = 1
    Int? cpu = 1
  }
  
  command <<<
    set -Eeuxo pipefail
    
    mkdir -p output
    
    mkdir -p output/
    mkdir -p docked/
    
    if [ -z "~{center_x}" ]; then \
      centers_x="$(autodockvina.get_search_grid_centers -i ~{input_file_receptor} -d x -s ~{size_x})"; else \
      centers_x=(~{center_x}); fi
    
    if [ -z "~{center_y}" ]; then \
      centers_y="$(autodockvina.get_search_grid_centers -i ~{input_file_receptor} -d y -s ~{size_y})"; else \
      centers_y=(~{center_y}); fi
      
    if [ -z "~{center_z}" ]; then \
      centers_z="$(autodockvina.get_search_grid_centers -i ~{input_file_receptor} -d z -s ~{size_z})"; else \
      centers_z=(~{center_z}); fi

    for center_x in ${centers_x}; do \
      for center_y in ${centers_y}; do \
        for center_z in ${centers_z}; do \
          for input_file_ligand in ~{sep=" " input_file_ligands}; do \
            vina --receptor ~{input_file_receptor} \
                 --ligand ${input_file_ligand} \
                 --center_x ${center_x} \
                 --center_y ${center_y} \
                 --center_z ${center_z} \
                 --size_x ~{size_x} \
                 --size_y ~{size_y} \
                 --size_z ~{size_z} \
                 --out docked/$(basename ${input_file_ligand})_${center_x}_${center_y}_${center_z} \
                 --num_modes ~{max_num_modes} \
                 --seed ~{seed} \
                 --cpu ~{cpu}; \
          done; \
        done;  \
      done; \
    done
    autodockvina.select_best_poses -i docked/* -o output/docked.pdbqt -n ~{max_num_modes} -e -6.0
    exit 0
  >>>
  
  runtime {
    cpu: "~{cpu}"
    memory: "2 GB"
    disks: "local-disk 40 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_autodockvina:1.1.2"
  }
  
  output {
    File output_file = "output/docked.pdbqt"
  }
  
}
