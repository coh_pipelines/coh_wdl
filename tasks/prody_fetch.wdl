version 1.0

workflow wf {
  call prody_fetch
}

task prody_fetch {

  input {
    Array[String] pdb_codes
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/
    
    cd output/
    prody fetch ${sep=" && prody fetch " pdb_codes}
  }
  
  runtime {
    cpu: "8"
    memory: "12 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_prody:20200309"
    preemptible: 5
  }
  
  output {
    Array[File] output_files = glob("output/*")
  }
  
}
