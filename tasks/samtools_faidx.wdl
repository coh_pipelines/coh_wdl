version 1.0

workflow wf {
  call samtools_faidx
}

task samtools_faidx {

  input {
    File input_file_fasta
  }
  
  command {
    set -Eeuxo pipefail
    mkdir -p output/
    
    samtools faidx ${input_file_fasta}
    mv ${input_file_fasta}.fai output/index.fai
  }
  
  runtime {
    cpu: "4"
    memory: "16GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_samtools:1.10"
    preemptible: 5
  }
  
  output {
    File output_file_fai = "output/index.fai"
  }
  
}
