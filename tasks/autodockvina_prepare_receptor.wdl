version 1.0

workflow wf {
  call autodockvina_prepare_receptor
}

task autodockvina_prepare_receptor {

  input {
    File input_file
  }
  
  command <<<
    set -Eeuxo pipefail
    
    mkdir -p output/
    /opt/mgltools_x86_64Linux2_1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_receptor4.py -r ~{input_file} -o output/receptor.pdbqt
  >>>
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 40 HDD"
    docker: "registry.gitlab.com/coh_docker_images/ubuntu_opt_mgltools:1.5.6"
    preemptible: 5
  }
  
  output {
    File output_file = "output/receptor.pdbqt"
  }
  
}
