version 1.0

workflow wf {
  call samtools_sort
}

task samtools_sort {

  input {
    File input_file_bam
    Int threads = 2
    String maximum_memory_per_thread = "4G"
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/
    mkdir -p temp/
    
    samtools sort \
      -T temp/ \
      -l 2 \
      -@ ${threads} \
      -m ${maximum_memory_per_thread} \
      --output-fmt BAM \
      -o "output/sorted.bam" \
      < ${input_file_bam}
  }
  
  runtime {
    cpu: "8"
    memory: "32 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_samtools:1.10"
    preemptible: 5
  }
  
  output {
    File output_file_bam = "output/sorted.bam"
  }
  
}
