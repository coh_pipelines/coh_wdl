version 1.0

workflow wf {
  call picard_validatesamfile
}

task picard_validatesamfile {

  input {
    File input_file
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/

    java -jar /opt/picard/picard.jar ValidateSamFile \
      I=${input_file} \
      MODE=SUMMARY > output/validatesamfile.txt || true
  }
  
  runtime {
    cpu: "4"
    memory: "16 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_picard:2.22.0"
    preemptible: 5
  }
  
  output {
    File output_file = "output/validatesamfile.txt"
  }
  
}


  