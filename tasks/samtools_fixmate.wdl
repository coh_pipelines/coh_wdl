version 1.0

workflow wf {
  call samtools_fixmate
}

task samtools_fixmate {

  input {
    File input_file_bam
  }
  
  command {
    set -Eeuxo pipefail
    mkdir -p output/
    
    samtools fixmate --threads 8 -O bam -m ${input_file_bam} output/fixmate.bam
  }
  
  runtime {
    cpu: "8"
    memory: "12 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_samtools:1.10"
    preemptible: 5
  }
  
  output {
    File output_file_bam = "output/fixmate.bam"
  }
  
}
