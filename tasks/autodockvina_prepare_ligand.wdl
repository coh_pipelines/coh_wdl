version 1.0

workflow wf {
  call autodockvina_prepare_ligand
}

task autodockvina_prepare_ligand {

  input {
    File input_file
  }
  
  command <<<
    set -Eeuxo pipefail
    
    mkdir -p output/
    /opt/mgltools_x86_64Linux2_1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_ligand4.py -l ~{input_file} -o output/ligand.pdbqt
  >>>
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 40 HDD"
    docker: "registry.gitlab.com/coh_docker_images/ubuntu_opt_mgltools:1.5.6"
    preemptible: 5
  }
  
  output {
    File output_file = "output/ligand.pdbqt"
  }
  
}
