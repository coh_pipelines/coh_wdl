version 1.0

workflow wf {
  call dwgsim
}

task dwgsim {

  input {
    File input_file
    Float? mutation_rate = 0.001
  }
  
  command {
    set -Eeuxo pipefail
    mkdir -p output/
    
    dwgsim -r ${mutation_rate} ${input_file} output/dwgsim
  }
  
  runtime {
    cpu: "8"
    memory: "12 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_dwgsim:20200309"
    preemptible: 5
  }
  
  output {
    Array[File] output_files = glob("output/*")
  }
  
}
