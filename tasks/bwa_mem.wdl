version 1.0

workflow wf {
  call bwa_mem
}

task bwa_mem {

  input {
    File input_file_fastq_reads1
    File input_file_fastq_reads2
    File input_file_reference_fasta
    File input_file_reference_bwa_index_amb
    File input_file_reference_bwa_index_ann
    File input_file_reference_bwa_index_bwt
    File input_file_reference_bwa_index_pac
    File input_file_reference_bwa_index_sa
    String? read_group_line
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/
    
    bwa mem \
      -v 3 \
      -Y \
      -K 100000000 \
      -t 9 \
      "${input_file_reference_fasta}" \
      "${input_file_fastq_reads1}" \
      "${input_file_fastq_reads2}" > output/aliged.bam \
  }
  
  runtime {
    cpu: "8"
    memory: "12 GB"
    disks: "local-disk 40 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_bwa:0.7.17"
    preemptible: 5
  }
  
  output {
    File output_file_bam = "output/aliged.bam"
  }
  
}
