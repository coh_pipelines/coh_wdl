version 1.0

workflow wf {
  call split
}

task split {

  input {
    File input_file
    Int lines_per_file
    Int suffix_length = 3 
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/

    split \
      -a ${suffix_length} \
      -l ${lines_per_file} \
      ${input_file} "output/$( basename ${input_file}.split_ )"
  }
  
  runtime {
    cpu: "1"
    memory: "32 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_split:20200309"
    preemptible: 5
  }
  
  output {
    Array[File] output_files = glob("output/*")
  }
  
}


  