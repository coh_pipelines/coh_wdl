version 1.0

workflow wf {
  call zdock
}

task zdock {

  input {
    File input_file_receptor
    File input_file_ligand
  }
  
  command <<<
    set -Eeuxo pipefail
    
    mkdir -p output/
    zdock -R ~{input_file_receptor} -L ~{input_file_ligand} -o output/zdock.out || true
    [[ -f output/zdock.out ]] || false
  >>>
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 40 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_zdock:3.0.2"
    preemptible: 5
  }
  
  output {
    File output_file = "output/zdock.out"
  }
  
}
