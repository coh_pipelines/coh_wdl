version 1.0

workflow wf {
  call gatk_collectrawwgsmetrics
}

task gatk_collectrawwgsmetrics {

  input {
    File input_file
    File input_file_reference_fasta
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/

    gatk CollectRawWgsMetrics \
      --java-options "-Xmx7G" \
      --USE_FAST_ALGORITHM false \
      --INPUT ${input_file} \
      --OUTPUT output/wgs_raw_metrics.txt \
      --REFERENCE_SEQUENCE ${input_file_reference_fasta} \
      --INCLUDE_BQ_HISTOGRAM true
  }
  
  runtime {
    cpu: "1"
    memory: "4 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_gatk:4.1.5.0"
    preemptible: 5
  }
  
  output {
    File output_file = "output/wgs_raw_metrics.txt"
  }
  
}


  