version 1.0

workflow wf {
  call modeller_generate_model
}

task modeller_generate_model {

  input {
    File input_file_ali
    Array[File] input_files_known_pdbs
  }
  
  command <<<
    set -Eeuxo pipefail
    
    mkdir -p output/
    
    modeller.generate_models -seq target -ali ~{input_file_ali} -k $(dirname ~{input_files_known_pdbs[0]})/ -nm 1 > generate_models_modeller_ouput.txt
    mkdir -p ../models/
    cp $( python -c "import os; print( os.path.basename( '~{input_file_ali}' ).replace('.ali','') )" )/00000/$( cat generate_models_modeller_ouput.txt | grep pdb | sort -nrk 3 | tail -n1 | cut -d" " -f1 ) "output/model.pdb"
  >>>
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_modeller:20200309"
    preemptible: 5
  }
  
  output {
    File output_file = "output/model.pdb"
  }
  
}
