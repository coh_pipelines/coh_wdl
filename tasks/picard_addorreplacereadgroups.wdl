version 1.0

workflow wf {
  call picard_addorreplacereadgroups
}

task picard_addorreplacereadgroups {

  input {
    File input_file
    String read_group_id
    String read_group_library
    String read_group_platform
    String read_group_platform_unit
    String read_group_sample_name
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/

    java -jar /opt/picard/picard.jar AddOrReplaceReadGroups \
      I=${input_file} \
      O=output/addorreplacereadgroups.bam \
      RGID=${read_group_id} \
      RGLB=${read_group_library} \
      RGPL=${read_group_platform} \
      RGPU=${read_group_platform_unit} \
      RGSM=${read_group_sample_name}
  }
  
  runtime {
    cpu: "4"
    memory: "16 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_picard:2.22.0"
    preemptible: 5
  }
  
  output {
    File output_file = "output/addorreplacereadgroups.bam"
  }
  
}


  