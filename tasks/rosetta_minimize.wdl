version 1.0

workflow wf {
  call rosetta_minimize
}

task rosetta_minimize {

  input {
    File input_file
    Int? num_cycles = 1
  }
  
  command <<<
    set -Eeuxo pipefail
    ls -lart /opt/bin/
    mkdir -p output/
    RosettaMinimize.py -c ~{num_cycles} -i ~{input_file} -o output/rosetta_min.pdb
  >>>
  
  runtime {
    cpu: "1"
    memory: "4 GB"
    disks: "local-disk 40 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_rosetta:3.11"
    preemptible: 5
  }
  
  output {
    File output_file = "output/rosetta_min.pdb"
  }
  
}
