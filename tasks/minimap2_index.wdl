version 1.0

workflow wf {
  call minimap2_index
}

task minimap2_index {

  input {
    File input_file_fasta
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/
    
    minimap2 -x map-ont -d output/index.mmi ${input_file_fasta}
  }
  
  runtime {
    cpu: "8"
    memory: "32 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_minimap2:20200309"
    preemptible: 5
  }
  
  output {
    File output_file_index = "output/index.mmi"
  }
  
}