version 1.0

workflow wf {
  call minimap2_align
}

task minimap2_align {

  input {
    File input_file_fastq_reads1
    File input_file_fastq_reads2
    File input_file_reference_mmi
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/
    
    minimap2 -a ${input_file_reference_mmi} ${input_file_fastq_reads1} ${input_file_fastq_reads2} > output/aligned.sam
  }
  
  runtime {
    cpu: "8"
    memory: "32 GB"
    disks: "local-disk 200 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_minimap2:20200309"
    preemptible: 5
  }
  
  output {
    File output_file_sam = "output/aligned.sam"
  }
  
}
