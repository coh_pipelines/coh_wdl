version 1.0

workflow wf {
  call samtools_index
}

task samtools_index {

  input {
    File input_file_bam
  }
  
  command {
    set -Eeuxo pipefail
    mkdir -p output/
    
    samtools index -@ 2 ${input_file_bam} output/index.bai
  }
  
  runtime {
    cpu: "4"
    memory: "16GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_samtools:1.10"
    preemptible: 5
  }
  
  output {
    File output_file_bai = "output/index.bai"
  }
  
}
