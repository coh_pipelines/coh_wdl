version 1.0

workflow wf {
  call test_simple
}

task test_simple {

  input {
    Int? value = 99
  }
  
  command <<<
    set -euxo pipefail
    
    mkdir -p output/
    echo "COMPLETE: ~{value}" > output/results.txt
    echo "OK"
    exit 0
  >>>
  
  runtime {
    cpu: "1"
    memory: "32 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_docker_images/ubuntu_base:20.04"
  }
  
  output {
    File output_file_index = "output/results.txt"
  }
  
}