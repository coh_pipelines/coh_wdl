version 1.0

workflow wf {
  call zdock_mark_sur
}

task zdock_mark_sur {

  input {
    File input_file
    Array[Int]? blocked_resnums
  }
  
  command <<<
    set -Eeuxo pipefail
    
    mkdir -p output/
    cp /opt/bin/uniCHARMM ./
    mark_sur ~{input_file} output/output.pdb
    BLOCKED_RESNUMS=(~{sep=' ' blocked_resnums})
    if [ "${#BLOCKED_RESNUMS[@]}" -ne "0" ]; then \
      ZdockMarkBlocked.py -i output/output.pdb -o output/output.pdb -b ~{sep=' ' blocked_resnums};
    fi
  >>>
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 40 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_zdock:3.0.2"
    preemptible: 5
  }
  
  output {
    File output_file = "output/output.pdb"
  }
  
}
