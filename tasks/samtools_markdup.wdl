version 1.0

workflow wf {
  call samtools_markdup
}

task samtools_markdup {

  input {
    File input_file_bam
    Int opt_dup_distance = 100
  }
  
  command {
    set -Eeuxo pipefail
    mkdir -p output/
    mkdir -p temp/
    
    samtools markdup \
      -d ${opt_dup_distance} \
      -S \
      -f "output/markdup.stats" \
      --threads 8 \
      --write-index \
      -T "temp/" \
      "${input_file_bam}" \
      "output/markdup.bam" 
  }
  
  runtime {
    cpu: "8"
    memory: "32 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_samtools:1.10"
    preemptible: 5
  }
  
  output {
    File output_file_bam = "output/markdup.bam"
    File output_file_stats = "output/markdup.stats"
  }
  
}
