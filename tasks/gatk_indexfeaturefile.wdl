version 1.0

workflow wf {
  call gatk_indexfeaturefile
}

task gatk_indexfeaturefile {

  input {
    File input_file
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/

    gatk IndexFeatureFile \
      --input ${input_file} \
      -O output/indexfeaturefile.tbi
  }
  
  runtime {
    cpu: "4"
    memory: "16 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_gatk:4.1.5.0"
    preemptible: 5
  }
  
  output {
    File output_file = "output/indexfeaturefile.tbi"
  }
  
}


  