version 1.0

workflow wf {
  call modeller_generate_ali
}

task modeller_generate_ali {

  input {
    File input_file_target_fasta
    Array[File] input_files_template_pdbs
    Array[String] input_chain_ids
  }
  
  command <<<
    set -Eeuxo pipefail
    
    mkdir -p output/knowns/
    
    TARGET_PROTEIN_SEQUENCE=$( cat ~{input_file_target_fasta} | sed "s|>.*||" | tr -d \\n )
    TARGET_PROTEIN_SEQUENCE_LENGTH=$( echo ${TARGET_PROTEIN_SEQUENCE} | wc -c )
    
    TEMPLATE_PDB_FILENAMES=(~{sep=' ' input_files_template_pdbs})
    TEMPLATE_CHAIN_IDS=(~{sep=' ' input_chain_ids})
    
    for index in ${!TEMPLATE_PDB_FILENAMES[*]}; do
      TEMPLATE_PDB_FILENAME=${TEMPLATE_PDB_FILENAMES[$index]}
      bioutils.extract_chains_and_sequences -i ${TEMPLATE_PDB_FILENAME}
      
      TEMPLATE_PDB_ID=$( python -c "import os; print( os.path.basename( '${TEMPLATE_PDB_FILENAME}' ).split('.')[0] )" )
      TEMPLATE_CHAIN_ID=${TEMPLATE_CHAIN_IDS[${index}]}
      TEMPLATE_NAME=${TEMPLATE_PDB_ID}_${TEMPLATE_CHAIN_ID}
      
      echo ">target
      ${TARGET_PROTEIN_SEQUENCE}" > target.${TEMPLATE_NAME}.seq
      
      TEMPLATE_PROTEIN_SEQUENCE_FILENAME=${TEMPLATE_NAME}.seq
      TEMPLATE_PROTEIN_SEQUENCE=$( cat ${TEMPLATE_PROTEIN_SEQUENCE_FILENAME} | sed "s|>.*||" | tr -d \\n )
      echo ">${TEMPLATE_NAME}
      ${TEMPLATE_PROTEIN_SEQUENCE}" >> target.${TEMPLATE_NAME}.seq
      TEMPLATE_PROTEIN_SEQUENCE_PAIR_ALIGNED=$( python -c "from Bio.Align.Applications import ClustalwCommandline; cline = ClustalwCommandline(\"clustalw2\", infile=\"target.${TEMPLATE_NAME}.seq\", endgaps=\"on\", output=\"fasta\"); print(cline); cline();" )
    done
    
    mkdir -p temp/
    bioutils.merge_pairwise_alignments -i *.fasta -o temp/all.fasta
    
    echo ">P1;target" > output/target.ali
    echo "sequence:target:1:A:${TARGET_PROTEIN_SEQUENCE_LENGTH}:A:::0.00: 0.00" >> output/target.ali
    TARGET_PROTEIN_SEQUENCE_ALIGNED=$( python -c "from Bio import AlignIO; alignment = AlignIO.read( \"temp/all.fasta\", \"fasta\" ); d = { a.id : a.seq for a in alignment }; print( d[\"target\"] );" )
    echo ${TARGET_PROTEIN_SEQUENCE_ALIGNED}"*" >> output/target.ali
    
    for index in ${!TEMPLATE_PDB_FILENAMES[*]}; do
      TEMPLATE_PDB_FILENAME=${TEMPLATE_PDB_FILENAMES[$index]}
      TEMPLATE_PDB_ID=$( python -c "import os; print( os.path.basename( '${TEMPLATE_PDB_FILENAME}' ).split('.')[0] )" )
      TEMPLATE_CHAIN_ID=${TEMPLATE_CHAIN_IDS[${index}]}
      TEMPLATE_NAME=${TEMPLATE_PDB_ID}_${TEMPLATE_CHAIN_ID}
      TEMPLATE_PROTEIN_PDB_FILENAME="${TEMPLATE_NAME}.pdb"
      TEMPLATE_PROTEIN_SEQUENCE_FIRST_RESNUM=$( python -c "import prody; atoms = prody.parsePDB(\"${TEMPLATE_PROTEIN_PDB_FILENAME}\", chain=\"${TEMPLATE_CHAIN_ID}\"); print( atoms.getResnums()[0] );" )
      TEMPLATE_PROTEIN_SEQUENCE_LAST_RESNUM=$( python -c "import prody; atoms = prody.parsePDB(\"${TEMPLATE_PROTEIN_PDB_FILENAME}\", chain=\"${TEMPLATE_CHAIN_ID}\"); print( atoms.getResnums()[-1] );" )
      echo "" >> output/target.ali
      echo ">P1;${TEMPLATE_NAME}" >> output/target.ali
      echo "structureX:${TEMPLATE_NAME}:${TEMPLATE_PROTEIN_SEQUENCE_FIRST_RESNUM}:${TEMPLATE_CHAIN_ID}:${TEMPLATE_PROTEIN_SEQUENCE_LAST_RESNUM}:${TEMPLATE_CHAIN_ID}:::0.00: 0.00" >> output/target.ali
      TEMPLATE_PROTEIN_SEQUENCE_ALIGNED=$( python -c "from Bio import AlignIO; alignment = AlignIO.read( \"temp/all.fasta\", \"fasta\" ); d = { a.id : a.seq for a in alignment }; print( d[\"${TEMPLATE_NAME}\"] );" )
      echo ${TEMPLATE_PROTEIN_SEQUENCE_ALIGNED}"*" >> output/target.ali
      cp ${TEMPLATE_PROTEIN_PDB_FILENAME} output/knowns/
    done
  >>>
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_bioutils:20200309"
    preemptible: 5
  }
  
  output {
    File output_file_ali = "output/target.ali"
    Array[File] output_files_knowns_pdbs = glob("output/knowns/*.pdb")
  }
  
}
