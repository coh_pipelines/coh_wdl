version 1.0

workflow wf {
  call bioutils_extract_chains_and_sequences
}

task bioutils_extract_chains_and_sequences {

  input {
    File input_file_pdb
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/
    
    cd output/
    bioutils.extract_chains_and_sequences -i ${input_file_pdb}
  }
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_bioutils:20200309"
    preemptible: 5
  }
  
  output {
    Array[File] output_files = glob("output/*")
  }
  
}
