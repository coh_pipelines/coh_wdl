version 1.0

workflow wf {
  call zdock_create
}

task zdock_create {

  input {
    File input_file_zdock
    File input_file_receptor
    File input_file_ligand
    Int? output_num_structures = 100
  }
  
  command <<<
    set -Eeuxo pipefail
    
    mkdir -p output/
    cd output/
    ln -s /opt/bin/create_lig ./
    cp ~{input_file_zdock} zdock.out
    cp ~{input_file_receptor} receptor.pdb
    cp ~{input_file_ligand} ligand.pdb
    ZdockModifyZdockOut.py -i zdock.out -r receptor.pdb -l ligand.pdb
    
    create.pl zdock.out ~{output_num_structures}
  >>>
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 40 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_zdock:3.0.2"
    preemptible: 5
  }
  
  output {
    Array[File] output_files = glob("output/complex*.pdb")
  }
  
}
