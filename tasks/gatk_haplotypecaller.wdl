version 1.0

workflow wf {
  call gatk_haplotypecaller
}

task gatk_haplotypecaller {

  input {
    File input_file
    File input_file_index
    File input_file_reference_fasta
    File input_file_reference_fai
    File input_file_reference_dict
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/

    gatk HaplotypeCaller \
      --java-options "-Xmx14G" \
      --reference ${input_file_reference_fasta} \
      --input ${input_file} \
      -ERC GVCF \
      -O "output/haplotypecaller.g.vcf.gz"
  }
  
  runtime {
    cpu: "4"
    memory: "16 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_gatk:4.1.5.0"
    preemptible: 5
  }
  
  output {
    File output_file = "output/haplotypecaller.g.vcf.gz"
  }
  
}


  