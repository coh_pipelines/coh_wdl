version 1.0

workflow wf {
  call verifybamid2
}

task verifybamid2 {

  input {
    File input_file_bam
    File input_file_ud
    File input_file_mu
    File input_file_bed
    File input_file_genome_reference
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/
    
    {# Some resources are included in the VerifyBamID home dir #}
    VERIFY_BAM_ID_HOME="$(dirname $(dirname $(which VerifyBamID)))"

    {# For some undocumented reason, there was a change between VerifyBamID 1.0.5 and 1.0.6 #}
    {# which results in the sanity check failing the process, there are a few work arounds #}
    {# we can either choose not to run ultra-low pass samples, revert back to 1.0.5, or disable #}
    {# the sanity check. The sanity check ensures that more than 10,000 markers were found. #}
    VerifyBamID \
      --NumThread 4 \
      --DisableSanityCheck \
      --UDPath "${VERIFY_BAM_ID_HOME}/resource/1000g.phase3.100k.b38.vcf.gz.dat.UD" \
      --MeanPath "${VERIFY_BAM_ID_HOME}/resource/1000g.phase3.100k.b38.vcf.gz.dat.mu" \
      --BedPath "${VERIFY_BAM_ID_HOME}/resource/1000g.phase3.100k.b38.vcf.gz.dat.bed" \
      --Reference "${input_file_genome_reference}"
      --BamFile "${input_file_bam}" \
      --Output "output/verifybamid2.txt"
  }
  
  runtime {
    cpu: "8"
    memory: "32 GB"
    disks: "local-disk 200 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_minimap2:20200309"
    preemptible: 5
  }
  
  output {
    File output_file_sam = "output/aligned.sam"
  }
  
}
