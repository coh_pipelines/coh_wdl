version 1.0

workflow wf {
  call gatk_collectwgsmetricswithnonzerocoverage
}

task gatk_collectwgsmetricswithnonzerocoverage {

  input {
    File input_file
    File input_file_reference_fasta
  }
  
  command {
    set -Eeuxo pipefail
    
    mkdir -p output/

    gatk CollectWgsMetricsWithNonZeroCoverage \
      --java-options "-Xmx7G" \
      --INPUT ${input_file} \
      --CHART_OUTPUT output/wgs_wnzc_metrics.pdf \
      --OUTPUT output/wgs_wnzc_metrics.txt \
      --REFERENCE_SEQUENCE ${input_file_reference_fasta}
  }
  
  runtime {
    cpu: "1"
    memory: "4 GB"
    disks: "local-disk 100 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_gatk:4.1.5.0"
    preemptible: 5
  }
  
  output {
    File output_file_txt = "output/wgs_wnzc_metrics.txt"
    File output_file_pdf = "output/wgs_wnzc_metrics.pdf"
  }
  
}


  