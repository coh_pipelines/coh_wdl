version 1.0

import "../tasks/prody_fetch.wdl"
import "../tasks/modeller_generate_ali.wdl"
import "../tasks/modeller_generate_model.wdl"

workflow generate_protein_model {

  input {
    File input_file_target_fasta
    Array[String] template_pdb_codes
    Array[String] template_chain_ids
  }

  call prody_fetch.prody_fetch as fetch_template_pdbs {
    input:
      pdb_codes = template_pdb_codes
  }
  
  call modeller_generate_ali.modeller_generate_ali as modeller_generate_target_ali {
    input:
      input_file_target_fasta = input_file_target_fasta,
      input_files_template_pdbs = fetch_template_pdbs.output_files,
      input_chain_ids = template_chain_ids
  }
  
  call modeller_generate_model.modeller_generate_model as modeller_generate_target_model {
    input:
      input_file_ali = modeller_generate_target_ali.output_file_ali,
      input_files_known_pdbs = modeller_generate_target_ali.output_files_knowns_pdbs
  }
  
  output {
    File output_model_pdb = modeller_generate_target_model.output_file
  }
  
  
}
