version 1.0

import "../tasks/zdock_mark_sur.wdl"
import "../tasks/zdock.wdl"
import "../tasks/zdock_create.wdl"
import "../tasks/rosetta_minimize.wdl"

workflow protein_protein_docking {

  input {
    File input_file_receptor_pdb
    File input_file_ligand_pdb
    Array[Int]? blocked_resnums_receptor
    Array[Int]? blocked_resnums_ligand
    Int? num_docked_structures = 100
    Int? num_rosetta_minimize_cycles = 1
  }

  call zdock_mark_sur.zdock_mark_sur as zdock_mark_sur_template {
    input:
      input_file = input_file_receptor_pdb,
      blocked_resnums = blocked_resnums_receptor
  }
  
  call zdock_mark_sur.zdock_mark_sur as zdock_mark_sur_ligand {
    input:
      input_file = input_file_ligand_pdb,
      blocked_resnums = blocked_resnums_ligand
  }
  
  call zdock.zdock as zdock_proteins {
    input:
      input_file_receptor = zdock_mark_sur_template.output_file,
      input_file_ligand = zdock_mark_sur_ligand.output_file
  }
  
  call zdock_create.zdock_create as zdock_create_docked_pdbs {
    input:
      input_file_zdock = zdock_proteins.output_file,
      input_file_receptor = zdock_mark_sur_template.output_file,
      input_file_ligand = zdock_mark_sur_ligand.output_file,
      output_num_structures = num_docked_structures
  }
  
  scatter ( zdock_create_docked_pdb_output_file in zdock_create_docked_pdbs.output_files ) {
    call rosetta_minimize.rosetta_minimize as rosetta_minimize_docked_pdbs {
      input:
        input_file = zdock_create_docked_pdb_output_file,
        num_cycles = num_rosetta_minimize_cycles
    }
  }
  
  output {
    File output_file_zdock_scores = zdock_proteins.output_file
    Array[File] output_files_pdbs = rosetta_minimize_docked_pdbs.output_file
  }
  
  
}
