version 1.0

task download_file {

  input {
    String url
  }
  
  command <<<
    set -euxo pipefail
    wget "~{url}" -O file
  >>>
  
  runtime {
    cpu: "1"
    memory: "2 GB"
    disks: "local-disk 200 HDD"
    docker: "registry.gitlab.com/coh_docker_images/alpine_tools:3.11.3"
  }
  
  output {
    File output_file = "file"
  }
  
}
