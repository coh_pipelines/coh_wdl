version 1.0

workflow wf {
  call samtools_merge
}

task hi {

  input {
    Array[File] input_files_bam
    Int threads = 2
  }
  
  command {
    set -Eeuxo pipefail
    mkdir -p output/
    
    samtools merge --threads ${threads} -c -f -l 6 \
      output/merged.bam \
      ${sep=" " input_files_bam}
  }
  
  runtime {
    cpu: "8"
    memory: "32 GB"
    disks: "local-disk 200 HDD"
    docker: "registry.gitlab.com/coh_pipelines/coh_wdl_samtools:1.10"
    preemptible: 5
  }
  
  output {
    File output_file_bam = "output/merged.bam"
  }
  
}
