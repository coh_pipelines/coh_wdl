version 1.0

import "./util/download_file.wdl"

workflow jetstream_resources {

  input {
    String url_grc_ncbi_readme = "ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/README_analysis_sets.txt"
    String url_primary_assembly_with_decoy_sequences = "ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_full_plus_hs38d1_analysis_set.fna.gz"
    String samtools_docker_image = "registry.gitlab.com/coh_docker_images/alpine_opt_samtools:1.10"
  }

  call download_file.download_file as download_grc_ncbi_readme {
    input:
      url = url_grc_ncbi_readme
  }

  call download_file.download_file as download_primary_assembly_with_decoy_sequences {
    input:
      url = url_primary_assembly_with_decoy_sequences
  }
  
  output {
    File output_file_1 = download_grc_ncbi_readme.output_file
    File output_file_2 = download_primary_assembly_with_decoy_sequences.output_file
  }
  
  
}
