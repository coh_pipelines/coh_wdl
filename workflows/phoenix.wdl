version 1.0

import "../tasks/bwa_mem.wdl"
import "../tasks/gatk_collectrawwgsmetrics.wdl"
import "../tasks/gatk_collectwgsmetrics.wdl"
import "../tasks/gatk_collectwgsmetricswithnonzerocoverage.wdl"
import "../tasks/samtools_fixmate.wdl"
import "../tasks/samtools_index.wdl"
import "../tasks/samtools_markdup.wdl"
import "../tasks/samtools_merge.wdl"
import "../tasks/samtools_sort.wdl"
import "../tasks/zcat_split.wdl"

workflow phoenix {

  input {
    File input_file_fastq_reads1
    File input_file_fastq_reads2
    File input_file_reference_fasta
    File input_file_reference_bwa_index_amb
    File input_file_reference_bwa_index_ann
    File input_file_reference_bwa_index_bwt
    File input_file_reference_bwa_index_pac
    File input_file_reference_bwa_index_sa
  }

  call zcat_split.zcat_split as split_fastq_reads1 {
    input:
      input_file = input_file_fastq_reads1,
      lines_per_file = 100000
  }
  
  call zcat_split.zcat_split as split_fastq_reads2 {
    input:
      input_file = input_file_fastq_reads2,
      lines_per_file = 100000
  }

  scatter( fastq_index in range( length( split_fastq_reads1.output_files ) ) ) {
    call bwa_mem.bwa_mem as align_fastq {
      input:
        input_file_fastq_reads1 = split_fastq_reads1.output_files[ fastq_index ],
        input_file_fastq_reads2 = split_fastq_reads2.output_files[ fastq_index ],
        input_file_reference_fasta = input_file_reference_fasta,
        input_file_reference_bwa_index_amb = input_file_reference_bwa_index_amb,
        input_file_reference_bwa_index_ann = input_file_reference_bwa_index_ann,
        input_file_reference_bwa_index_bwt = input_file_reference_bwa_index_bwt,
        input_file_reference_bwa_index_pac = input_file_reference_bwa_index_pac,
        input_file_reference_bwa_index_sa = input_file_reference_bwa_index_sa
    }
    call samtools_fixmate.samtools_fixmate as fixmate_alignment {
      input:
        input_file_bam = align_fastq.output_file_bam
    }
    call samtools_sort.samtools_sort as sort_alignment {
      input:
        input_file_bam = fixmate_alignment.output_file_bam
    }
  }
  
  call samtools_merge.samtools_merge as merge_alignments {
    input:
      input_files_bam = sort_alignment.output_file_bam
  }
  
  call samtools_markdup.samtools_markdup as markdup_alignment {
    input:
      input_file_bam = merge_alignments.output_file_bam
  }
  
  call samtools_index.samtools_index as index_alignment {
    input:
      input_file_bam = markdup_alignment.output_file_bam
  }

  call gatk_collectwgsmetrics.gatk_collectwgsmetrics as gatk_collectwgsmetrics {
    input:
      input_file = markdup_alignment.output_file_bam,
      input_file_reference_fasta = input_file_reference_fasta
  }
  
  call gatk_collectwgsmetricswithnonzerocoverage.gatk_collectwgsmetricswithnonzerocoverage as gatk_collectwgsmetricswithnonzerocoverage {
    input:
      input_file = markdup_alignment.output_file_bam,
      input_file_reference_fasta = input_file_reference_fasta
  }
  
  call gatk_collectrawwgsmetrics.gatk_collectrawwgsmetrics as gatk_collectrawwgsmetrics {
    input:
      input_file = markdup_alignment.output_file_bam,
      input_file_reference_fasta = input_file_reference_fasta
  }
  
  output {
    File output_file_alignment_bam = markdup_alignment.output_file_bam
    File output_file_alignment_index = index_alignment.output_file_bai
    File output_file_markdup_stats = markdup_alignment.output_file_stats
  }
  
  
}
